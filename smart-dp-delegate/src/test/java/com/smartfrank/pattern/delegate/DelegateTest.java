package com.smartfrank.pattern.delegate;

import com.smartfrank.pattern.delegate.example.TestController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Description: 委派模式测试用例
 * <br/>
 * DelegateTest
 *
 * @author laiql
 * @date 2021/11/15 15:55
 */
@Slf4j
public class DelegateTest {

    @Test
    public void test() {
        TestController testController = new TestController();
        testController.test("url");
        System.out.println("===============");
        testController.test("method");
    }
}
