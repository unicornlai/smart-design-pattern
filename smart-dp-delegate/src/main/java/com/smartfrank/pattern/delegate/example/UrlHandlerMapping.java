package com.smartfrank.pattern.delegate.example;

/**
 * Description: url路径处理映射
 * <br/>
 * UrlHandlerMapping
 *
 * @author laiql
 * @date 2021/11/15 16:53
 */
public class UrlHandlerMapping implements HandlerMapping {
    @Override
    public HandlerMapping getHandler() {
        System.out.println("UrlHandlerMapping.getHandler");
        return null;
    }
}
