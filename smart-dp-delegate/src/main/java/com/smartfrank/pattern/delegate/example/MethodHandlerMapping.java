package com.smartfrank.pattern.delegate.example;

/**
 * Description: 请求方法处理映射
 * <br/>
 * MethodHandlerMapping
 *
 * @author laiql
 * @date 2021/11/15 16:55
 */
public class MethodHandlerMapping implements HandlerMapping {
    @Override
    public HandlerMapping getHandler() {
        System.out.println("MethodHandlerMapping.getHandler");
        return null;
    }
}
