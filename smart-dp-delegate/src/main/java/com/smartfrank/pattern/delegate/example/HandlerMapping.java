package com.smartfrank.pattern.delegate.example;

/**
 * Description: 抽象handlerMapping接口
 * <br/>
 * HandlerMapping
 *
 * @author laiql
 * @date 2021/11/15 17:47
 */
public interface HandlerMapping {
    HandlerMapping getHandler();
}
