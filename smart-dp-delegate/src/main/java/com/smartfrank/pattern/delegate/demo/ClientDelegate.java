package com.smartfrank.pattern.delegate.demo;

/**
 * Description: 客户端类
 * <br/>
 * ClientDelegate
 *
 * @author laiql
 * @date 2021/11/15 16:09
 */
public class ClientDelegate {
    public static void main(String[] args) {
        new Delegate().doTask();
    }
}
