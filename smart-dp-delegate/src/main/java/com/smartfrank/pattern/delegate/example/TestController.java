package com.smartfrank.pattern.delegate.example;

/**
 * Description: 测试Controller
 * <br/>
 * TestController
 *
 * @author laiql
 * @date 2021/11/15 17:07
 */
public class TestController {

    public String test(String handler) {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        dispatcherServlet.doDispatch(handler);
        return "模拟调用处理器映射器";
    }
}
