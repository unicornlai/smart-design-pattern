package com.smartfrank.pattern.delegate.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: 请求中央处理器
 * <br/>
 * DispatcherServlet
 *
 * @author laiql
 * @date 2021/11/15 16:58
 */
public class DispatcherServlet {

    private Map<String, HandlerMapping> handlerMappings;

    public DispatcherServlet() {
        this.handlerMappings = new HashMap<>(2);
        this.initHandlerMappings();
    }

    /**
     * 初始化HandlerMapping
     */
    private void initHandlerMappings() {
        this.handlerMappings.put("method", new MethodHandlerMapping());
        this.handlerMappings.put("url", new UrlHandlerMapping());
    }

    /**
     * 模拟具体调度方法
     *
     * @param handlerMapping 处理映射器
     */
    public void doDispatch(String handlerMapping) {
        this.handlerMappings.get(handlerMapping).getHandler();
    }
}
