package com.smartfrank.pattern.delegate.demo;

/**
 * Description: 抽象Task接口
 * <br/>
 * Task
 *
 * @author laiql
 * @date 2021/11/15 16:03
 */
public interface Task {
    void doTask();
}
