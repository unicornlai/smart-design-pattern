package com.smartfrank.pattern.delegate.demo;

/**
 * Description: 执行任务A
 * <br/>
 * ConcreteTaskA
 *
 * @author laiql
 * @date 2021/11/15 16:04
 */
public class ConcreteTaskA implements Task {
    @Override
    public void doTask() {
        System.out.println("执行任务 A操作");
    }
}
