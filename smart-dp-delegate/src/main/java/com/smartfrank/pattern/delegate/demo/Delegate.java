package com.smartfrank.pattern.delegate.demo;

import java.util.Random;

/**
 * Description: 委派任务对象
 * <br/>
 * Delegate
 *
 * @author laiql
 * @date 2021/11/15 16:05
 */
public class Delegate implements Task {
    /**
     * 根据业务委派具体执行对象
     */
    @Override
    public void doTask() {
        System.out.println("全权代理模式开始执行...");
        Task task = null;
        //可以使用策略模式优化
        if (new Random().nextBoolean()) {
            task = new ConcreteTaskA();
            task.doTask();
        } else {
            task = new ConcreteTaskB();
            task.doTask();
        }
        System.out.println("全权代理模式执行完成...");
    }
}
