package com.smartfrank.pattern.combination.example.model.aggregates;


import com.smartfrank.pattern.combination.example.model.TreeNode;
import com.smartfrank.pattern.combination.example.model.TreeRoot;

import java.util.Map;

/**
 * Description: 规则树聚合对象
 * <br/>
 * TreeRich
 *
 * @author laiql
 * @date 2021/12/3 14:29
 */
public class TreeRich {

    /**
     * 树根信息
     */
    private TreeRoot treeRoot;
    /**
     * 树节点ID -> 子节点
     */
    private Map<Long, TreeNode> treeNodeMap;

    public TreeRich(TreeRoot treeRoot, Map<Long, TreeNode> treeNodeMap) {
        this.treeRoot = treeRoot;
        this.treeNodeMap = treeNodeMap;
    }

    public TreeRoot getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(TreeRoot treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Map<Long, TreeNode> getTreeNodeMap() {
        return treeNodeMap;
    }

    public void setTreeNodeMap(Map<Long, TreeNode> treeNodeMap) {
        this.treeNodeMap = treeNodeMap;
    }
}
