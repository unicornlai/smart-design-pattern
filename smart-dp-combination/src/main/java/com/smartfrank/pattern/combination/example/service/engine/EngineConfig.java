package com.smartfrank.pattern.combination.example.service.engine;

import com.smartfrank.pattern.combination.example.service.logic.LogicFilter;
import com.smartfrank.pattern.combination.example.service.logic.impl.UserAgeFilter;
import com.smartfrank.pattern.combination.example.service.logic.impl.UserGenderFilter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Description:  决策节点配置
 * <br/>
 * EngineConfig
 *
 * @author laiql
 * @date 2021/12/3 14:53
 */
public class EngineConfig {
    static Map<String, LogicFilter> logicFilterMap;

    static {
        logicFilterMap = new ConcurrentHashMap<>();
        logicFilterMap.put("userAge",new UserAgeFilter());
        logicFilterMap.put("userGender",new UserGenderFilter());
    }

    public Map<String, LogicFilter> getLogicFilterMap() {
        return logicFilterMap;
    }

    public void setLogicFilterMap(Map<String, LogicFilter> logicFilterMap) {
        EngineConfig.logicFilterMap = logicFilterMap;
    }
}
