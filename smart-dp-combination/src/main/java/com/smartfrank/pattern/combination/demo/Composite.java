package com.smartfrank.pattern.combination.demo;

import java.util.ArrayList;

/**
 * Description: 树枝构件
 * <br/>
 * Composite
 *
 * @author laiql
 * @date 2021/12/3 10:53
 */
public class Composite extends Component {
    private ArrayList<Component> children = new ArrayList<Component>();

    @Override
    public void add(Component component) {
        children.add(component);
    }

    @Override
    public void remove(Component component) {
        children.remove(component);
    }

    @Override
    public Component getChild(int index) {
        return children.get(index);
    }

    @Override
    public void operation() {
        for (Component child : children) {
            child.operation();
        }
    }
}
