package com.smartfrank.pattern.combination.example.service.engine.impl;


import com.smartfrank.pattern.combination.example.model.EngineResult;
import com.smartfrank.pattern.combination.example.model.TreeNode;
import com.smartfrank.pattern.combination.example.model.aggregates.TreeRich;
import com.smartfrank.pattern.combination.example.service.engine.EngineBase;

import java.util.Map;

/**
 * Description: 决策引擎处理器
 * <br/>
 * TreeEngineHandle
 *
 * @author laiql
 * @date 2021/12/3 15:00
 */
public class TreeEngineHandle extends EngineBase {

    @Override
    public EngineResult process(Long treeId, String userId, TreeRich treeRich, Map<String, String> decisionMatter) {
        // 决策流程
        TreeNode treeNode = engineDecisionMaker(treeRich, treeId, userId, decisionMatter);
        // 决策结果
        return new EngineResult(userId, treeId, treeNode.getTreeNodeId(), treeNode.getNodeValue());
    }
}
