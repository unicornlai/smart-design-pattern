package com.smartfrank.pattern.combination.demo;

/**
 * Description: 树叶构件
 * <br/>
 * Leaf
 *
 * @author laiql
 * @date 2021/12/3 10:52
 */
public class Leaf extends Component {

    private String name;

    public Leaf(String name) {
        this.name = name;
    }

    @Override
    public void operation() {
        System.out.println("树叶" + name + "：被访问！");
    }
}
