package com.smartfrank.pattern.combination.example.service.logic.impl;

import com.smartfrank.pattern.combination.example.service.logic.BaseLogic;

import java.util.Map;

/**
 * Description: 年龄节点过滤
 * <br/>
 * UserAgeFilter
 *
 * @author laiql
 * @date 2021/12/3 14:48
 */
public class UserAgeFilter extends BaseLogic {
    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("age");
    }
}
