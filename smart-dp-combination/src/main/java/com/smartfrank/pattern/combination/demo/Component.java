package com.smartfrank.pattern.combination.demo;

/**
 * Description: 抽象构建
 * <br/>
 * Component
 *
 * @author laiql
 * @date 2021/12/3 10:48
 */
public abstract class Component {

    public void add(Component component) {
        throw new UnsupportedOperationException("禁止操作");
    }

    public void remove(Component component) {
        throw new UnsupportedOperationException("禁止操作");
    }

    public Component getChild(int index) {
        throw new UnsupportedOperationException("禁止操作");
    }

    /**
     * 抽象打印方法留给子类实现
     */
    public abstract void operation();
}
