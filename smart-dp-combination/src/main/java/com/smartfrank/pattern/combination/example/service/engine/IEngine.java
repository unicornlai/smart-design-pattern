package com.smartfrank.pattern.combination.example.service.engine;

import com.smartfrank.pattern.combination.example.model.EngineResult;
import com.smartfrank.pattern.combination.example.model.aggregates.TreeRich;

import java.util.Map;

/**
 * Description: 决策引擎流程
 * <br/>
 * IEngine
 *
 * @author laiql
 * @date 2021/12/3 14:53
 */
public interface IEngine {

    EngineResult process(final Long treeId, final String userId, TreeRich treeRich, final Map<String, String> decisionMatter);

}