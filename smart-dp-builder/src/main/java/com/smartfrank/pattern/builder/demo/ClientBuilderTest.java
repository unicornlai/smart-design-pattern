package com.smartfrank.pattern.builder.demo;

/**
 * Description: 建造者测试客户端
 * <br/>
 * ClientBuilderTest
 *
 * @author laiql
 * @date 2021/11/26 11:17
 */
public class ClientBuilderTest {

    public static void main(String[] args) {
        //苹果测试
        Director directorApple = new Director(new ConcreteAppleBuilder());
        directorApple.construct().show();
        //橘子测试
        Director directorOrange = new Director(new ConcreteOrangeBuilder());
        directorOrange.construct().show();
    }
}
