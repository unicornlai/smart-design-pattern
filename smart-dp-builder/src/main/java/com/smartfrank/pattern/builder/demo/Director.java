package com.smartfrank.pattern.builder.demo;

/**
 * Description: 指挥者：调用建造者中的方法完成复杂对象的创建
 * <br/>
 * Director
 *
 * @author laiql
 * @date 2021/11/26 11:16
 */
public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    //产品构建与组装方法
    public Product construct() {
        builder.buildName();
        builder.buildPrice();
        builder.buildOrigin();
        return builder.getProduct();
    }
}
