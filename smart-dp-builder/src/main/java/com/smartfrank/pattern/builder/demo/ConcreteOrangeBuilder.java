package com.smartfrank.pattern.builder.demo;

import java.math.BigDecimal;

/**
 * Description: 具体商品建造者
 * <br/>
 * ConcreteOrangeBuilder
 *
 * @author laiql
 * @date 2021/11/26 11:31
 */
public class ConcreteOrangeBuilder extends Builder {
    @Override
    public void buildName() {
        product.setName("Orange");
    }

    @Override
    public void buildPrice() {
        product.setPrice(new BigDecimal("10"));
    }

    @Override
    public void buildOrigin() {
        product.setOrigin("中国云南");
    }
}
