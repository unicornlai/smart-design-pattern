package com.smartfrank.pattern.builder.demo;

import java.math.BigDecimal;

/**
 * Description: 具体商品建造者
 * <br/>
 * ConcreteAppleBuilder
 *
 * @author laiql
 * @date 2021/11/26 11:31
 */
public class ConcreteAppleBuilder extends Builder {
    @Override
    public void buildName() {
        product.setName("Apple");
    }

    @Override
    public void buildPrice() {
        product.setPrice(new BigDecimal("5888"));
    }

    @Override
    public void buildOrigin() {
        product.setOrigin("美国");
    }
}
