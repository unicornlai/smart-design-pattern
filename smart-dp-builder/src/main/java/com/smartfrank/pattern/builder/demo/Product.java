package com.smartfrank.pattern.builder.demo;

import java.math.BigDecimal;

/**
 * Description: 产品对象
 * <br/>
 * Product
 *
 * @author laiql
 * @date 2021/11/26 11:10
 */
public class Product {
    /**
     * 产品名
     */
    private String name;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 产地
     */
    private String origin;

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * 显示产品特性
     */
    public void show() {
        System.out.println("商品：" + name + "价格：" + price + "产地：" + origin);
    }
}
