package com.smartfrank.pattern.builder.demo;

/**
 * Description: 抽象构造者对象
 * <br/>
 * Builder
 *
 * @author laiql
 * @date 2021/11/26 11:13
 */
public abstract class Builder {
    //创建产品对象
    protected Product product = new Product();

    public abstract void buildName();

    public abstract void buildPrice();

    public abstract void buildOrigin();

    public Product getProduct() {
        return product;
    }
}
