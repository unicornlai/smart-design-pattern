package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 活动中
 * <br/>
 * DoingState
 *
 * @author laiql
 * @date 2021/11/10 10:10
 */
public class DoingState extends AbstractState {
    @Override
    public Result activityClosed(Context context) {
        ActivityInfo activityInfo = context.getActivityInfo();
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), Status.Close);
        return new Result("0000", "活动关闭成功");
    }
}
