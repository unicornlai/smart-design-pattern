package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 待审核状态
 * <br/>
 * CheckState
 *
 * @author laiql
 * @date 2021/11/10 9:50
 */
public class CheckState extends AbstractState {

    @Override
    public Result examinationPassed(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Pass);
        return new Result("0000", "活动审核通过完成");
    }

    @Override
    public Result reviewRejected(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Refuse);
        return new Result("0000", "活动审核撤销回到编辑中");
    }

    @Override
    public Result withdrawalOfTrial(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Editing);
        return super.withdrawalOfTrial(context);
    }

    @Override
    public Result activityClosed(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Close);
        return new Result("0000", "活动审核关闭完成");
    }

    /**
     * 执行活动状态流转状态
     *
     * @param activityInfo 活动信息
     * @param status       状态
     */
    private void extractedExecStatus(ActivityInfo activityInfo, Status status) {
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), status);
    }
}
