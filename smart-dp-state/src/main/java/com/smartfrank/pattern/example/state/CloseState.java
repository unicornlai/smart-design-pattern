package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 活动关闭
 * <br/>
 * CloseState
 *
 * @author laiql
 * @date 2021/11/10 10:12
 */
public class CloseState extends AbstractState {

    @Override
    public Result activityOn(Context context) {
        ActivityInfo activityInfo = context.getActivityInfo();
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), Status.Open);
        return new Result("0000", "活动开启完成");
    }
}
