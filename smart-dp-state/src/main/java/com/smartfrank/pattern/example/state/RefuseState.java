package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 活动拒绝
 * <br/>
 * RefuseState
 *
 * @author laiql
 * @date 2021/11/10 10:05
 */
public class RefuseState extends AbstractState {
    @Override
    public Result reviewRejected(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Refuse);
        return new Result("0000", "活动审核拒绝不可重复审核");
    }

    @Override
    public Result withdrawalOfTrial(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Editing);
        return new Result("0000", "撤销审核完成");
    }

    @Override
    public Result activityClosed(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Close);
        return new Result("0000", "活动审核关闭完成");
    }

    /**
     * 执行活动状态流转状态
     *
     * @param activityInfo 活动信息
     * @param status       状态
     */
    private void extractedExecStatus(ActivityInfo activityInfo, Status status) {
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), status);
    }
}
