package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 审核通过
 * <br/>
 * PassState
 *
 * @author laiql
 * @date 2021/11/10 10:02
 */
public class PassState extends AbstractState {

    @Override
    public Result reviewRejected(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Refuse);
        return new Result("0000", "活动审核拒绝完成");
    }

    @Override
    public Result activityClosed(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Close);
        return new Result("0000", "活动审核关闭完成");
    }

    @Override
    public Result activityExecution(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Doing);
        return new Result("0000", "活动变更活动中完成");
    }

    /**
     * 执行活动状态流转状态
     *
     * @param activityInfo 活动信息
     * @param status       状态
     */
    private void extractedExecStatus(ActivityInfo activityInfo, Status status) {
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), status);
    }
}
