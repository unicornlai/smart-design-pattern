package com.smartfrank.pattern.example;

/**
 * Description: 环境上下文
 * <br/>
 * Context
 *
 * @author laiql
 * @date 2021/11/9 8:40 下午
 */
public class Context extends AbstractState {
    private State state;
    private ActivityInfo activityInfo;

    public Context(State state, ActivityInfo activityInfo) {
        this.state = state;
        this.activityInfo = activityInfo;
    }

    /**
     * 获取当前状态
     *
     * @return
     */
    public State getState() {
        return state;
    }

    /**
     * 设置流转状态
     *
     * @param state 下一个状态
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * 获取当前活动信息
     *
     * @return
     */
    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    /**
     * 设置活动信息
     *
     * @param activityInfo
     */
    public void setActivityInfo(ActivityInfo activityInfo) {
        this.activityInfo = activityInfo;
    }

    @Override
    public Result activityReview(Context context) {
        return this.state.activityReview(context);
    }

    @Override
    public Result examinationPassed(Context context) {
        return this.state.examinationPassed(context);
    }

    @Override
    public Result reviewRejected(Context context) {
        return this.state.reviewRejected(context);
    }

    @Override
    public Result withdrawalOfTrial(Context context) {
        return this.state.withdrawalOfTrial(context);
    }

    @Override
    public Result activityClosed(Context context) {
        return this.state.activityClosed(context);
    }

    @Override
    public Result activityOn(Context context) {
        return this.state.activityOn(context);
    }

    @Override
    public Result activityExecution(Context context) {
        return this.state.activityExecution(context);
    }
}
