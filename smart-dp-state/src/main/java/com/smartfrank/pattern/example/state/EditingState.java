package com.smartfrank.pattern.example.state;

import com.smartfrank.pattern.example.*;
import com.smartfrank.pattern.example.service.ActivityService;

/**
 * Description: 编辑提审核
 * <br/>
 * EditingState
 *
 * @author laiql
 * @date 2021/11/9 9:47 下午
 */
public class EditingState extends AbstractState {
    @Override
    public Result activityReview(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Check);
        return new Result("0000", "活动提审成功");
    }

    @Override
    public Result activityClosed(Context context) {
        extractedExecStatus(context.getActivityInfo(), Status.Close);
        return new Result("0000", "活动关闭成功");
    }

    /**
     * 执行活动状态流转状态
     *
     * @param activityInfo 活动信息
     * @param status       状态
     */
    private void extractedExecStatus(ActivityInfo activityInfo, Status status) {
        ActivityService.execStatus(activityInfo.getActivityId(), activityInfo.getStatus(), status);
    }
}
