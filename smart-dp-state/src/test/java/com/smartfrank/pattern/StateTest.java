package com.smartfrank.pattern;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.smartfrank.pattern.example.ActivityInfo;
import com.smartfrank.pattern.example.Context;
import com.smartfrank.pattern.example.Result;
import com.smartfrank.pattern.example.Status;
import com.smartfrank.pattern.example.service.ActivityService;
import com.smartfrank.pattern.example.state.CloseState;
import com.smartfrank.pattern.example.state.EditingState;
import com.smartfrank.pattern.example.state.OpenState;
import com.smartfrank.pattern.example.state.PassState;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Description: 状态模式测试用例
 * <br/>
 * StateTest
 *
 * @author laiql
 * @date 2021/11/10 10:16
 */
@Slf4j
public class StateTest {

    @Test
    public void stateTest() {
        String activityId = "100001";
        ActivityInfo activityInfo = ActivityService.init(activityId, Status.Editing);
        Context context = new Context(new EditingState(), activityInfo);
        Result result = context.activityReview(context);

        log.info("测试结果(编辑中To提审活动)：{}", JSONUtil.toJsonStr(result));
        log.info("活动信息：{}", JSONUtil.toJsonStr(ActivityService.queryActivityInfo(activityId)));

        context.setState(new PassState());
        result = context.activityClosed(context);
        log.info("测试结果(提审活动To审核成功)：{}", JSONUtil.toJsonStr(result));
        log.info("活动信息：{}", JSONUtil.toJsonStr(ActivityService.queryActivityInfo(activityId)));

        result = context.activityExecution(context);
        log.info("测试结果(审核成功To开启活动中)：{}", JSONUtil.toJsonStr(result));
        log.info("活动信息：{}", JSONUtil.toJsonStr(ActivityService.queryActivityInfo(activityId)));

    }
}
